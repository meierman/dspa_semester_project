package socialnet;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import piper.Like;

import java.util.Collections;

public class LikeKafkaConsumer extends CustomKafkaConsumer{
    KafkaConsumer<String, Like> consumer;

    public LikeKafkaConsumer(String groupName, String topic){

        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupName);

        CustomDeSerializer<Like> likeDeser = new CustomDeSerializer<Like>(Like.class);
        consumer = new KafkaConsumer<String, Like>(props, new StringDeserializer(), likeDeser);
        consumer.subscribe(Collections.singletonList(topic));
    }

    public void readLikes(){

        ConsumerRecords<String, Like> messages = consumer.poll(100);
        for (ConsumerRecord<String, Like> m : messages) {

            if (m == null || m.value() == null){
                // There was an error in deserialization
                continue;
            }

            System.out.println("Like Message received " + m.toString());
            Like l = m.value();
            System.out.println("IngestionDate " + l.getIngestionDate().toString());
        }

    }
}