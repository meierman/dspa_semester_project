package socialnet;

import piper.*;

import org.apache.kafka.common.serialization.StringSerializer;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.KafkaProducer;

public class PostKafkaProducer  extends CustomKafkaProducer{

    public Producer<String, Post> producer;

    public PostKafkaProducer(){
        CustomDeSerializer<Post> postSerializer = new CustomDeSerializer<Post>(Post.class);
        producer = new KafkaProducer<String, Post>(props, new StringSerializer(), postSerializer);
    }

    public void sendPost(Post p, String topic){
        ProducerRecord<String, Post> record = new ProducerRecord<>(topic, p);
        producer.send(record);
    }

}
