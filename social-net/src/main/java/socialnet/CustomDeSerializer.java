package socialnet;

import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.common.serialization.DeserializationSchema;
import org.apache.flink.api.common.serialization.SerializationSchema;
import org.apache.flink.api.common.serialization.TypeInformationSerializationSchema;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.typeutils.TypeExtractor;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serializer;

import java.io.IOException;
import java.util.Map;


public class CustomDeSerializer<T> implements Deserializer<T>, DeserializationSchema<T>,
        Serializer<T>, SerializationSchema<T> {

    final Class<T> type;
    final TypeInformation<T> info;
    final TypeInformationSerializationSchema<T> schema;

    public CustomDeSerializer(Class type){
        this.type = type;
        info = TypeExtractor.createTypeInfo(type);
        schema = new TypeInformationSerializationSchema<T>(info, new ExecutionConfig());
    }

    // Deserializer
    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
    }
    @Override
    public T deserialize(String topic, byte[] data) {
        return this.deserialize(data);
    }
    @Override
    public void close() {
    }

    //DeserializationSchema
    @Override
    public T deserialize(byte[] data) {
        T object = null;
        try {
            object = schema.deserialize(data);
        } catch (Exception exception) {
            System.out.println("Error in deserializing bytes "+ exception);
        }
        return object;
    }

    @Override
    public boolean isEndOfStream(T t) {
        return false;
    }

    @Override
    public TypeInformation<T> getProducedType() {
        return TypeExtractor.getForClass(type);
    }

    // Serializer
    @Override
    public byte[] serialize(String topic, T data) {
        return this.serialize(data);
    }

    // SerializationSchema
    @Override
    public byte[] serialize(T data) {
        byte[] retVal = null;
        try {
            retVal = schema.serialize(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retVal;
    }


}
