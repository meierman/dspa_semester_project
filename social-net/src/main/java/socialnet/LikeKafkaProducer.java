package socialnet;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import piper.Like;

public class LikeKafkaProducer extends CustomKafkaProducer{

    public Producer<String, Like> producer;

    public LikeKafkaProducer(){
        CustomDeSerializer<Like> likeSerializer = new CustomDeSerializer<Like>(Like.class);
        producer = new KafkaProducer<String, Like>(props, new StringSerializer(), likeSerializer);
    }

    public void sendLike(Like l, String topic){
        ProducerRecord<String, Like> record = new ProducerRecord<>(topic, l);
        producer.send(record);
    }

}
