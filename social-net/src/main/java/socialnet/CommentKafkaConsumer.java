package socialnet;

import org.apache.kafka.clients.consumer.*;
import piper.*;
import org.apache.kafka.common.serialization.StringDeserializer;
import java.util.Collections;


public class CommentKafkaConsumer extends CustomKafkaConsumer{
    KafkaConsumer<String, Comment> consumer;

    public CommentKafkaConsumer(String groupName, String topic){

        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupName);

        CustomDeSerializer<Comment> commentDeser = new CustomDeSerializer<Comment>(Comment.class);
        consumer = new KafkaConsumer<String, Comment>(props, new StringDeserializer(), commentDeser);
        consumer.subscribe(Collections.singletonList(topic));
    }

    public void readComments(){

        ConsumerRecords<String, Comment> messages = consumer.poll(100);
        for (ConsumerRecord<String, Comment> m : messages) {

            if (m == null || m.value() == null){
                // There was an error in deserialization
                continue;
            }

            System.out.println("Comment Message received " + m.toString());
            Comment c = m.value();
            System.out.println("IngestionTime " + c.getIngestionDate().toString());
        }

    }
}


