package socialnet;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import piper.*;

import java.util.Collections;

public class PostKafkaConsumer extends CustomKafkaConsumer{
    KafkaConsumer<String, Post> consumer;

    public PostKafkaConsumer(String groupName, String topic){

        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupName);

        CustomDeSerializer<Post> postDeser = new CustomDeSerializer<Post>(Post.class);
        consumer = new KafkaConsumer<String, Post>(props, new StringDeserializer(), postDeser);
        consumer.subscribe(Collections.singletonList(topic));
    }

    public void readPosts(){

        ConsumerRecords<String, Post> messages = consumer.poll(100);
        for (ConsumerRecord<String, Post> m : messages) {

            if (m == null || m.value() == null){
                // There was an error in deserialization
                continue;
            }

            System.out.println("Post Message received " + m.toString());
            Post p = m.value();
            System.out.println("IngestionDate " + p.getIngestionDate().toString());
        }

    }
}
