package socialnet;


public interface IKafkaConstants {

    public static String KAFKA_BROKERS = "localhost:9092";
    public static String ZOOKEEPER_CONNECTION = "localhost:2181";

    public static String TOPIC_UNORDERED_COMMENTS = "unordered_comments";
    public static String TOPIC_UNORDERED_POSTS = "unordered_posts";
    public static String TOPIC_UNORDERED_LIKES = "unordered_likes";

    public static String TOPIC_COMMENTS = "comments";
    public static String TOPIC_POSTS = "posts";
    public static String TOPIC_LIKES = "likes";

}
