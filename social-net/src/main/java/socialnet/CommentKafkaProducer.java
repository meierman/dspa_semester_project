package socialnet;

import piper.*;

import org.apache.kafka.common.serialization.StringSerializer;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.KafkaProducer;


public class CommentKafkaProducer extends CustomKafkaProducer{

    public Producer<String, Comment> producer;

    public CommentKafkaProducer(){
        CustomDeSerializer<Comment> commentSerializer = new CustomDeSerializer<Comment>(Comment.class);
        producer = new KafkaProducer<String, Comment>(props, new StringSerializer(), commentSerializer);
    }

    public void sendComment(Comment c, String topic){
        ProducerRecord<String, Comment> record = new ProducerRecord<>(topic, c);
        producer.send(record);
    }

}
