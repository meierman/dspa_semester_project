package streamer;

import java.util.HashMap;
import java.util.Properties;
import java.util.SortedMap;
import java.util.concurrent.locks.ReentrantLock;


class EventListener extends Thread {

    static Properties kafkaProps;
    static SortedMap<Integer, PostStatistic> postIdToStat;
    static HashMap<Integer, Integer> postIdToOriginPostId;
    static ReentrantLock apsLock;
    static Swapper swapper;

    static final int SPEED_UP = 1800;

    static final int MAX_DELAY_MS = 10;



    static boolean find_parent_post(Integer post_id){
        // Note: caller is holding apsLock

        if (postIdToOriginPostId.containsKey(post_id)){
            // return with lock
            return true;
        }

        apsLock.unlock();

        boolean found_in_db = false;
        try {
            found_in_db = swapper.try_load_post(post_id);
        } catch (Exception e){
            e.printStackTrace();
        }
        finally {
            // get lock back
            apsLock.lock();
        }


        if (found_in_db){
            System.out.println("*** Post ID " + post_id + " restored from DB ***");
            // return with lock
            return true;
        }

        // it might be that the child post arrived earlier than the parent post
        int n_retries = 0;
        int max_retries = 3;

        while (!postIdToOriginPostId.containsKey(post_id)) {
            // we need to wait for parent post
            apsLock.unlock();
            System.out.println("*** Post ID " + post_id + "not available now ***");

            try {
                currentThread().sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            apsLock.lock();

            n_retries++;
            if (n_retries >= max_retries) {
                //return with lock
                return false;
            }

        }

        // return with lock
        return true;
    }
}