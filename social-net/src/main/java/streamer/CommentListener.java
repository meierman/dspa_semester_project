package streamer;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;
import piper.Comment;
import socialnet.CustomDeSerializer;
import socialnet.IKafkaConstants;

import java.util.HashMap;
import java.util.Properties;
import java.util.SortedMap;
import java.util.concurrent.locks.ReentrantLock;





class CommentListener extends EventListener {

    static StreamExecutionEnvironment env;

    public CommentListener (Properties kafkaProps,
                            SortedMap<Integer, PostStatistic> postIdToStat,
                            HashMap<Integer, Integer> postIdToOriginPostId,
                            ReentrantLock apsLock,
                            Swapper swapper) {

        this.kafkaProps = kafkaProps;
        this.postIdToStat = postIdToStat;
        this.postIdToOriginPostId = postIdToOriginPostId;
        this.apsLock = apsLock;
        this.swapper = swapper;
    }


    private static void process_comments(){

        CustomDeSerializer<Comment> CommentDeser = new CustomDeSerializer<Comment>(Comment.class);

        FlinkKafkaConsumer011<Comment> commentConsumer = new FlinkKafkaConsumer011<Comment>(
                IKafkaConstants.TOPIC_COMMENTS,
                CommentDeser,
                kafkaProps);

        DataStream<Comment> stream = env.addSource(commentConsumer);
        System.out.println("ActivePostStats Comment Consumer added");

        stream
                .map(new MapFunction<Comment, Comment>() {
                @Override
                public Comment map(Comment c) {
                    System.out.println("New comment arrived. " + c.getId());

                    // can be reply to post or reply to comment
                    boolean isReplyToPost = true;
                    int post_id = c.getReply_to_postId();

                    if (post_id < 0){

                        isReplyToPost = false;
                        post_id = c.getReply_to_commentId();

                        assert (post_id >= 0);
                    }

                    System.out.println("Comment is reply to " + post_id);

                    String outString = "";
                    apsLock.lock();
                    try {
                        // Wait until parent post has arrived
                        if (find_parent_post(post_id)) {

                            int orig_post_id = postIdToOriginPostId.getOrDefault(post_id, -1);

                            PostStatistic ps = postIdToStat.get(orig_post_id);

                            if (isReplyToPost) {
                                ps.incrementComments(c.getPersonId(), c.getCreationDate());
                            } else {
                                ps.incrementReplies(c.getPersonId(), c.getCreationDate());
                            }

                            ps.addPerson(c.getPersonId(), c.getCreationDate());

                            // update origin post id map
                            int comment_id = c.getId();
                            postIdToOriginPostId.put(comment_id, orig_post_id);

                            outString = "### Comment Update: Post " + orig_post_id + ": " + ps.getnComments() + " C, " + ps.getnReplies()
                                    + " R, " + ps.getnUniquePeopleEngaged() + " UP";

                        } else {
                            System.out.println("*** Comment " + c.getId() +
                                    " could not find parent Post ID " + post_id + " ***");

                        }

                    } finally {
                        apsLock.unlock();
                    }

                    System.out.println(outString);
                    return c;
                }
            });
    }


    public void run() {

        env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        process_comments();

        try {
            env.execute("Flink Streaming APS COMMENT");
        } catch (Exception e){

        }
    }
}

