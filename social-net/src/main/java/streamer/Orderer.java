package streamer;

import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.TimerService;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer011;
import org.apache.flink.util.Collector;
import piper.Comment;
import piper.IPiperConstants;
import piper.Like;
import piper.Post;
import socialnet.CustomDeSerializer;
import socialnet.IKafkaConstants;

import java.util.PriorityQueue;
import java.util.Properties;


class PostSortFunction extends KeyedProcessFunction<Integer, Post, Post> {


    private ValueState<PriorityQueue<Post>> queueState = null;


    @Override
    public void open(Configuration config) {
        ValueStateDescriptor<PriorityQueue<Post>> descriptor = new ValueStateDescriptor<>(
                // state name
                "sorted-events",
                // type information of state
                TypeInformation.of(new TypeHint<PriorityQueue<Post>>() {
                }));
        queueState = getRuntimeContext().getState(descriptor);


    }

    @Override
    public void processElement(Post event, Context context, Collector<Post> out) throws Exception {
        TimerService timerService = context.timerService();

        if (context.timestamp() > timerService.currentWatermark()) {
            PriorityQueue<Post> queue = queueState.value();
            if (queue == null) {
                queue = new PriorityQueue<>(10);
            }
            queue.add(event);
            queueState.update(queue);
            timerService.registerEventTimeTimer(event.getCreationDate().getTimeInMillis());
        }
    }

    @Override
    public void onTimer(long timestamp, OnTimerContext context, Collector<Post> out) throws Exception {
        PriorityQueue<Post> queue = queueState.value();
        Long watermark = context.timerService().currentWatermark();
        Post head = queue.peek();
        while (head != null && head.getCreationDate().getTimeInMillis() <= watermark) {
            out.collect(head);
            queue.remove(head);
            head = queue.peek();
        }
    }

}


class CommentSortFunction extends KeyedProcessFunction<Integer, Comment, Comment> {


    private ValueState<PriorityQueue<Comment>> queueState = null;


    @Override
    public void open(Configuration config) {
        ValueStateDescriptor<PriorityQueue<Comment>> descriptor = new ValueStateDescriptor<>(
                // state name
                "sorted-events",
                // type information of state
                TypeInformation.of(new TypeHint<PriorityQueue<Comment>>() {
                }));
        queueState = getRuntimeContext().getState(descriptor);


    }

    @Override
    public void processElement(Comment event, Context context, Collector<Comment> out) throws Exception {
        TimerService timerService = context.timerService();

        if (context.timestamp() > timerService.currentWatermark()) {
            PriorityQueue<Comment> queue = queueState.value();
            if (queue == null) {
                queue = new PriorityQueue<>(30);
            }
            queue.add(event);
            queueState.update(queue);
            timerService.registerEventTimeTimer(event.getCreationDate().getTimeInMillis());
        }
    }

    @Override
    public void onTimer(long timestamp, OnTimerContext context, Collector<Comment> out) throws Exception {
        PriorityQueue<Comment> queue = queueState.value();
        Long watermark = context.timerService().currentWatermark();
        Comment head = queue.peek();
        while (head != null && head.getCreationDate().getTimeInMillis() <= watermark) {
            out.collect(head);
            queue.remove(head);
            head = queue.peek();
        }
    }

}


class LikeSortFunction extends KeyedProcessFunction<Integer, Like, Like> {


    private ValueState<PriorityQueue<Like>> queueState = null;


    @Override
    public void open(Configuration config) {
        ValueStateDescriptor<PriorityQueue<Like>> descriptor = new ValueStateDescriptor<>(
                // state name
                "sorted-events",
                // type information of state
                TypeInformation.of(new TypeHint<PriorityQueue<Like>>() {
                }));
        queueState = getRuntimeContext().getState(descriptor);


    }

    @Override
    public void processElement(Like event, Context context, Collector<Like> out) throws Exception {
        TimerService timerService = context.timerService();

        if (context.timestamp() > timerService.currentWatermark()) {
            PriorityQueue<Like> queue = queueState.value();
            if (queue == null) {
                queue = new PriorityQueue<>(30);
            }
            queue.add(event);
            queueState.update(queue);
            timerService.registerEventTimeTimer(event.getCreationDate().getTimeInMillis());
        }
    }

    @Override
    public void onTimer(long timestamp, OnTimerContext context, Collector<Like> out) throws Exception {
        PriorityQueue<Like> queue = queueState.value();
        Long watermark = context.timerService().currentWatermark();
        Like head = queue.peek();
        while (head != null && head.getCreationDate().getTimeInMillis() <= watermark) {
            out.collect(head);
            queue.remove(head);
            head = queue.peek();
        }
    }

}




public class Orderer extends Thread {
    static StreamExecutionEnvironment env;
    static Properties kafkaProps = new Properties();
    static int MAX_DELAY_MS;

    public Orderer(int max_delay_ms){

        this.MAX_DELAY_MS = max_delay_ms / IPiperConstants.SPEED_UP + 1;

        kafkaProps.setProperty("zookeeper.connect", IKafkaConstants.ZOOKEEPER_CONNECTION);
        kafkaProps.setProperty("bootstrap.servers", IKafkaConstants.KAFKA_BROKERS);
        // always read the Kafka topic from the start
        kafkaProps.setProperty("auto.offset.reset", "earliest");
    }

    private void do_ordering(){

        CustomDeSerializer<Post> PostDeser = new CustomDeSerializer<Post>(Post.class);

        FlinkKafkaConsumer011<Post> postConsumer = new FlinkKafkaConsumer011<Post>(
                IKafkaConstants.TOPIC_UNORDERED_POSTS,
                PostDeser,
                kafkaProps);

        FlinkKafkaProducer011<Post> postProducer = new FlinkKafkaProducer011<Post>(
                IKafkaConstants.TOPIC_POSTS,
                PostDeser,
                kafkaProps);

        DataStream<Post> postStream = env.addSource(postConsumer);
        postStream
                .assignTimestampsAndWatermarks(
                new BoundedOutOfOrdernessTimestampExtractor<Post>(Time.milliseconds(MAX_DELAY_MS)) {

                    @Override
                    public long extractTimestamp(Post element) {
                        return element.getCreationDate().getTimeInMillis();
                    }
                })
                .keyBy(Post::getKey)
                .process(new PostSortFunction())
                .addSink(postProducer);


        // Comments
        CustomDeSerializer<Comment> CommentDeser = new CustomDeSerializer<Comment>(Comment.class);

        FlinkKafkaConsumer011<Comment> commentConsumer = new FlinkKafkaConsumer011<Comment>(
                IKafkaConstants.TOPIC_UNORDERED_COMMENTS,
                CommentDeser,
                kafkaProps);

        FlinkKafkaProducer011<Comment> commentProducer = new FlinkKafkaProducer011<Comment>(
                IKafkaConstants.TOPIC_COMMENTS,
                CommentDeser,
                kafkaProps);

        DataStream<Comment> commentStream = env.addSource(commentConsumer);

        commentStream
                .assignTimestampsAndWatermarks(
                        new BoundedOutOfOrdernessTimestampExtractor<Comment>(Time.milliseconds(MAX_DELAY_MS)) {

                            @Override
                            public long extractTimestamp(Comment element) {
                                return element.getCreationDate().getTimeInMillis();
                            }
                        })
                .keyBy(Comment::getKey)
                .process(new CommentSortFunction());


         commentStream.addSink(commentProducer);


        // Likes

        CustomDeSerializer<Like> LikeDeser = new CustomDeSerializer<Like>(Like.class);

        FlinkKafkaConsumer011<Like> likeConsumer = new FlinkKafkaConsumer011<Like>(
                IKafkaConstants.TOPIC_UNORDERED_LIKES,
                LikeDeser,
                kafkaProps);


        FlinkKafkaProducer011<Like> likeProducer = new FlinkKafkaProducer011<Like>(
                IKafkaConstants.TOPIC_LIKES,
                LikeDeser,
                kafkaProps);

        DataStream<Like> likeStream = env.addSource(likeConsumer);

        likeStream
                .assignTimestampsAndWatermarks(
                        new BoundedOutOfOrdernessTimestampExtractor<Like>(Time.milliseconds(MAX_DELAY_MS)) {

                            @Override
                            public long extractTimestamp(Like element) {
                                return element.getCreationDate().getTimeInMillis();
                            }
                        })
                .keyBy(Like::getKey)
                .process(new LikeSortFunction())
                .addSink(likeProducer);

    }


    public void run(){

        env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);

        do_ordering();

        try {
            env.execute("Flink Streaming Reodering");
        } catch (Exception e){

        }

    }
}
