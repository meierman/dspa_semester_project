package streamer;


import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;
import piper.Post;
import socialnet.CustomDeSerializer;
import socialnet.IKafkaConstants;

import java.util.*;
import java.util.concurrent.locks.ReentrantLock;


class PostListener extends EventListener {

    static StreamExecutionEnvironment env;
    static Calendar simulatedStart;
    static Calendar actualStart;

    public PostListener (Properties kafkaProps,
                         SortedMap<Integer, PostStatistic> postIdToStat,
                         HashMap<Integer, Integer> postIdToOriginPostId,
                         Calendar simulatedStart,
                         Calendar actualStart,
                         ReentrantLock apsLock) {

        this.kafkaProps = kafkaProps;
        this.postIdToStat = postIdToStat;
        this.postIdToOriginPostId = postIdToOriginPostId;
        this.simulatedStart = simulatedStart;
        this.actualStart = actualStart;
        this.apsLock = apsLock;
    }


    private static void process_posts(){

        CustomDeSerializer<Post> PostDeser = new CustomDeSerializer<Post>(Post.class);


        FlinkKafkaConsumer011<Post> postConsumer = new FlinkKafkaConsumer011<Post>(
                IKafkaConstants.TOPIC_POSTS,
                PostDeser,
                kafkaProps);

        DataStream<Post> stream = env.addSource(postConsumer);
        System.out.println("ActivePostStats Post Consumer added");

        stream
                .map(new MapFunction<Post, Post>() {
                @Override
                public Post map(Post p) {
                    System.out.println("New Post arrived. " + p.getId());

                    //first post must write simulated start time
                    if (simulatedStart.getTimeInMillis() == 0){
                        simulatedStart.setTime(p.getCreationDate().getTime());
                        //actualStart.setTimeInMillis(System.currentTimeMillis());

                        System.out.println("Setting start time to " + p.getCreationDate().getTimeInMillis());
                    }

                    int post_id = p.getId();

                    String outString = "";
                    apsLock.lock();
                    try {
                        // setup post statistics
                        postIdToStat.putIfAbsent(post_id, new PostStatistic());
                        PostStatistic ps = postIdToStat.get(post_id);

                        ps.addPerson(p.getPersonId(), p.getCreationDate());

                        // set post_id as origin_post_id
                        postIdToOriginPostId.putIfAbsent(post_id, post_id);

                        outString = "### Post Update: Post " + post_id + ": " + ps.getnComments() + " C, "
                                + ps.getnReplies() + " R, " + ps.getnUniquePeopleEngaged() + " UP";
                    } finally {
                        apsLock.unlock();
                    }

                    System.out.println(outString);

                    return p;
                }
            });

    }

    public void run() {
        System.out.println("Post Listener Started");

        env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        process_posts();

        try {
            env.execute("Flink Streaming APS POST");
        } catch (Exception e){

        }

    }
}

