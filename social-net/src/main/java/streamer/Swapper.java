package streamer;


import java.sql.*;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

class Swapper extends APS_Worker{

    static Connection con;
    static Statement statement;

    public Swapper(SortedMap<Integer, PostStatistic> postIdToStat,
                   HashMap<Integer, Integer> postIdToOriginPostId,
                   ReentrantLock apsLock,
                   Calendar simulatedStart,
                   Calendar actualStart,
                   int speed_up) {

        this.postIdToStat = postIdToStat;
        this.postIdToOriginPostId = postIdToOriginPostId;
        this.apsLock = apsLock;
        this.simulatedStart = simulatedStart;
        this.actualStart = actualStart;
        this.SPEED_UP = speed_up;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost/aps?user=root");
            statement = con.createStatement();

            statement.executeUpdate("DROP TABLE IF EXISTS aps.orig");

            String ct = "CREATE TABLE IF NOT EXISTS aps.orig (" +
                    "id int NOT NULL, " +
                    "orig int NOT NULL, " +
                    "PRIMARY KEY (id) );";

            statement.executeUpdate(ct);

            statement.executeUpdate("DROP TABLE IF EXISTS aps.stat");

            ct =    "CREATE TABLE IF NOT EXISTS aps.stat (" +
                    "id int NOT NULL, " +
                    "n_comments int NOT NULL, " +
                    "n_replies int NOT NULL, " +
                    "last_update bigint NOT NULL, " +
                    "PRIMARY KEY (id) )";

            statement.executeUpdate(ct);

            statement.executeUpdate("DROP TABLE IF EXISTS aps.engaged");


            ct =    "CREATE TABLE IF NOT EXISTS aps.engaged (" +
                    "id int NOT NULL, " +
                    "pers int NOT NULL, " +
                    "last_update bigint NOT NULL, " +
                    "PRIMARY KEY (id, pers) )";

            statement.executeUpdate(ct);

        } catch (ClassNotFoundException e1) {
            e1.printStackTrace();
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }

    static void db_insert_orig(int post_id, int orig_id) throws SQLException {
        String insert = "INSERT IGNORE INTO orig VALUES (" +post_id+ ", " +orig_id+ ") ;";
        statement.executeUpdate(insert);
    }

    static void db_insert_stat(int post_id, int n_comments, int n_replies, long last_update) throws SQLException {
        String insert = "REPLACE INTO stat " +
                "VALUES (" + post_id+ ", " +n_comments+ ", " +n_replies+ ", " + last_update + ");";
        statement.executeUpdate(insert);
    }

    static void db_insert_engaged(int post_id, int pers_id, long last_update) throws SQLException {
        String insert = "REPLACE INTO engaged VALUES (" +post_id+ ", " +pers_id+ ", " + last_update+ ");";
        statement.executeUpdate(insert);
    }

    public void swap_out_inactive_posts() {

        if (simulatedStart.getTimeInMillis() == 0){
            // not ready yet
            return;
        }

        List<Integer> postsToSwapOut = new ArrayList<>();
        List<Integer> referecesToSwapOut = new ArrayList<>();

        apsLock.lock();
//        System.out.println("Im swapping out now.");
        try{

            long simulatedTime = simulatedStart.getTimeInMillis() +
                    (System.currentTimeMillis() - actualStart.getTimeInMillis())*SPEED_UP;


            for (Map.Entry<Integer, PostStatistic> entry : postIdToStat.entrySet()) {

                Integer post_id = entry.getKey();
                PostStatistic ps = entry.getValue();

                // // if post is old put out this stat and all postToOrig entries with this postId as orig to db
                long timeSinceUpdate = simulatedTime - ps.getLastUpdate().getTimeInMillis();
                if ( timeSinceUpdate > 12 * 3600 * 1000){


                    postsToSwapOut.add(post_id);
                    db_insert_stat(post_id, ps.getnComments(), ps.getnReplies(), ps.getLastUpdate().getTimeInMillis());

                    for (Integer person: ps.getAllPeopleEngaged()){
                        db_insert_engaged(post_id, person, ps.getLastInteraction(person).getTimeInMillis());
                    }
                }
            }

            for (Map.Entry<Integer,Integer> entry: postIdToOriginPostId.entrySet()){
                Integer post_id = entry.getKey();
                Integer orig_post_id = entry.getValue();

                if (postsToSwapOut.contains(orig_post_id)){
                    db_insert_orig(post_id, orig_post_id);
                    referecesToSwapOut.add(post_id);
                }
            }

            // delete swap entries from current state
            for (Integer el:postsToSwapOut){
                postIdToStat.remove(el);
            }

            for (Integer el:referecesToSwapOut){
                postIdToOriginPostId.remove(el);
            }


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            apsLock.unlock();
        }

    }


    public boolean try_load_post(Integer post_id){

        ResultSet rs= null;
        Integer orig_post_id = 0;

        // check if post is in database
        try {
            rs = statement.executeQuery("SELECT * FROM orig WHERE id = " + post_id + ";");

            if (rs.next()) {
                // post exists
                orig_post_id = rs.getInt("orig");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (orig_post_id == 0){
            // post is not in db
            System.out.println("Parent post of post " + post_id + " ist not in database.");
            return false;
        }

        apsLock.lock();
        System.out.println("Im swapping in post " + orig_post_id + " now.");
        try {

            int n_comments = 0, n_replies = 0;
            long last_update = 0;

            List<Integer> people_engaged = new ArrayList<Integer>();
            List<Long> people_last_interaction = new ArrayList<Long>();

            // load post statistic
            String stat_query = "SELECT * FROM stat WHERE id = " + orig_post_id + ";";
            rs = statement.executeQuery(stat_query);
            while(rs.next()){

                n_comments = rs.getInt("n_comments");
                n_replies = rs.getInt("n_replies");
                last_update = rs.getLong("last_update");
            }

            String engaged_query = "SELECT * FROM engaged WHERE id = " + orig_post_id + ";";
            rs = statement.executeQuery(engaged_query);
            while(rs.next()){

                people_engaged.add(rs.getInt("pers"));
                people_last_interaction.add(rs.getLong("last_update"));

            }

            // setup post statistics

            postIdToStat.putIfAbsent(orig_post_id, new PostStatistic(n_comments, n_replies, last_update,
                    people_engaged, people_last_interaction));

            // load child post ids
            String orig_query = "SELECT * FROM orig WHERE orig = " + orig_post_id + ";";
            rs = statement.executeQuery(orig_query);
            while(rs.next()){

                postIdToOriginPostId.putIfAbsent(rs.getInt("id"), orig_post_id);

            }


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            apsLock.unlock();
        }

        return true;
    }
}

