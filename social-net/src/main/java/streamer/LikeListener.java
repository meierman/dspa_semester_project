package streamer;


import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;
import piper.Like;
import socialnet.CustomDeSerializer;
import socialnet.IKafkaConstants;

import java.util.HashMap;
import java.util.Properties;
import java.util.SortedMap;
import java.util.concurrent.locks.ReentrantLock;



class LikeListener extends EventListener{

    static StreamExecutionEnvironment env;

    public LikeListener (Properties kafkaProps,
                         SortedMap<Integer, PostStatistic> postIdToStat,
                         HashMap<Integer, Integer> postIdToOriginPostId,
                         ReentrantLock apsLock,
                         Swapper swapper) {

        this.kafkaProps = kafkaProps;
        this.postIdToStat = postIdToStat;
        this.postIdToOriginPostId = postIdToOriginPostId;
        this.apsLock = apsLock;
        this.swapper = swapper;

    }

    private static void process_likes(){

        CustomDeSerializer<Like> LikeDeser = new CustomDeSerializer<Like>(Like.class);

        FlinkKafkaConsumer011<Like> likeConsumer = new FlinkKafkaConsumer011<Like>(
                IKafkaConstants.TOPIC_LIKES,
                LikeDeser,
                kafkaProps);

        DataStream<Like> stream = env.addSource(likeConsumer);
        System.out.println("ActivePostStats Like Consumer added");

        stream
                .map(new MapFunction<Like, Like>() {
                @Override
                public Like map(Like l) {
                    System.out.println("New Like arrived. " + l.getPersonId() + " | " + l.getPostId());

                    int post_id = l.getPostId();

                    String outString = "";
                    apsLock.lock();
                    try {
                        if (find_parent_post(post_id)){

                            int orig_post_id = postIdToOriginPostId.getOrDefault(post_id, -1);
                            PostStatistic ps = postIdToStat.get(orig_post_id);

                            ps.addPerson(l.getPersonId(), l.getCreationDate());

                            outString = "### Like Update: Post " + orig_post_id + ": " + ps.getnComments() + " C, "
                                    + ps.getnReplies() + " R, " + ps.getnUniquePeopleEngaged() + " UP";

                        } else {
                            System.out.println("*** Like from user " + l.getPersonId() +
                                    " could not find parent Post ID " + post_id + " ***");
                        }

                    } finally {
                        apsLock.unlock();
                    }

                    System.out.println(outString);
                    return l;
                }
            });
    }

    public void run() {

        env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        process_likes();

        try {
            env.execute("Flink Streaming APS LIKE");
        } catch (Exception e){

        }

    }
}

