package streamer;

import java.util.*;

class PostStatistic implements Cloneable
{
    private int nComments = 0;
    private int nReplies = 0;
    private Set<Integer> uniquePeopleEngaged = new HashSet<Integer>();
    private Calendar lastUpdate;
    private HashMap<Integer, Calendar> userIdToLastPostInteraction = new HashMap<Integer, Calendar>();

    PostStatistic () {}

    PostStatistic (int nComments, int nReplies, long lastUpdate,
                   List<Integer> people_engaged, List<Long> people_last_interaction){

        this.nComments = nComments;
        this.nReplies = nReplies;
        this.lastUpdate = Calendar.getInstance();
        this.lastUpdate.setTimeInMillis(lastUpdate);

        int nPeople = people_engaged.size();
        for (int i=0; i<nPeople; ++i){
            uniquePeopleEngaged.add(people_engaged.get(i));

            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(people_last_interaction.get(i));
            userIdToLastPostInteraction.putIfAbsent(people_engaged.get(i),c);
        }
    }


    public int getnComments() {
        return nComments;
    }

    public int getnReplies() {
        return nReplies;
    }

    public int getnUniquePeopleEngaged() {
        return uniquePeopleEngaged.size();
    }

    public Calendar getLastUpdate() {
        return  lastUpdate;
    }

    public Integer[] getAllPeopleEngaged(){
        return uniquePeopleEngaged.toArray(new Integer[0]);
    }

    public Calendar getLastInteraction(Integer userId){
        // returns null if user not present
        return userIdToLastPostInteraction.get(userId);
    }

    synchronized void incrementComments(Integer userId, Calendar creationTime) {
        nComments ++;
        updateInteractionTimestamps(userId, creationTime);
    }

    synchronized void incrementReplies(Integer userId, Calendar creationTime) {
        nReplies ++;
        updateInteractionTimestamps(userId, creationTime);
    }

    synchronized void addPerson(Integer userId, Calendar creationTime) {
        uniquePeopleEngaged.add(userId);
        updateInteractionTimestamps(userId, creationTime);
    }

    private synchronized void setLastUpdate(Calendar creationTime){
        if (lastUpdate == null || lastUpdate.compareTo(creationTime) < 0){
            lastUpdate = creationTime;
        }
    }

    private synchronized void updateInteractionTimestamps(Integer userId, Calendar creationTime){
        setLastUpdate(creationTime);

        userIdToLastPostInteraction.putIfAbsent(userId, creationTime);
        if (userIdToLastPostInteraction.get(userId).compareTo(creationTime) < 0 ){
            userIdToLastPostInteraction.put(userId, creationTime);
        }

    }

    public PostStatistic clone() throws CloneNotSupportedException {
        return (PostStatistic) super.clone();
    }

    public void setValuesFromDatabase(int nComments, int nReplies, long lastUpdate,
                                      List<Integer> people_engaged, List<Long> people_last_interaction){

        System.out.println("*** From Database *** " +
                "C: " + nComments + " R: " + nReplies + " LU: " + lastUpdate +
                " PE: " + people_engaged.size() + " LI: " + people_last_interaction.size());



    }

}