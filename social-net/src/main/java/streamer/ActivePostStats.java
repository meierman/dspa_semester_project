package streamer;

import org.apache.commons.io.FileUtils;
import socialnet.IKafkaConstants;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;


public class ActivePostStats extends APS_Worker {


    static Properties kafkaProps = new Properties();
    static Swapper swapper;


    public ActivePostStats(int speed_up){

        this.postIdToStat = new TreeMap<Integer, PostStatistic>();
        this.postIdToOriginPostId = new HashMap<Integer, Integer>();
        this.apsLock = new ReentrantLock();
        this.simulatedStart = Calendar.getInstance();
        this.actualStart = Calendar.getInstance();
        this.SPEED_UP = speed_up;

        this.kafkaProps = new Properties();

        this.simulatedStart.setTimeInMillis(0);

    }


    private static void do_setup(){
        kafkaProps.setProperty("zookeeper.connect", IKafkaConstants.ZOOKEEPER_CONNECTION);
        kafkaProps.setProperty("bootstrap.servers", IKafkaConstants.KAFKA_BROKERS);
        // always read the Kafka topic from the start
        kafkaProps.setProperty("auto.offset.reset", "earliest");

        swapper = new Swapper(postIdToStat, postIdToOriginPostId, apsLock, simulatedStart,
                actualStart, SPEED_UP);
    }


    private static void start_event_listener() {

        PostListener postListener = new PostListener(kafkaProps, postIdToStat, postIdToOriginPostId,
                simulatedStart, actualStart, apsLock);
        postListener.start();

        CommentListener commentListener = new CommentListener(kafkaProps, postIdToStat, postIdToOriginPostId,
                apsLock, swapper);
        commentListener.start();

        LikeListener likeListener = new LikeListener(kafkaProps, postIdToStat, postIdToOriginPostId,
                apsLock, swapper);
        likeListener.start();

    }


    public static String convert_ms(long millisec) {
        long seconds=millisec/1000;
        long s = seconds % 60;
        long m = (seconds / 60) % 60;
        long h = (seconds / (60 * 60));
        return String.format("%d:%02d:%02d", h,m,s);
    }


    private static void get_aps_state_copy(List<Integer> postIds, List<PostStatistic> postStats){
        apsLock.lock();
        try {
            for (Map.Entry<Integer, PostStatistic> entry : postIdToStat.entrySet()){

                Integer post_id = entry.getKey();
                PostStatistic ps = entry.getValue();

                postIds.add(post_id);
                postStats.add(ps.clone());
            }

        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        } finally {
            apsLock.unlock();
        }
    }

    private static String generate_aps_content(){

        if (simulatedStart.getTimeInMillis() == 0){
            // not ready yet
            return "";
        }

        List<Integer> postIds = new ArrayList<Integer>();
        List<PostStatistic> postStats = new ArrayList<PostStatistic>();

        get_aps_state_copy(postIds, postStats);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        long simulatedTime = simulatedStart.getTimeInMillis() +
                (System.currentTimeMillis() - actualStart.getTimeInMillis())*SPEED_UP;

        StringBuilder activeBuilder = new StringBuilder();
        StringBuilder inactiveBuilder = new StringBuilder();

        String Title = "C: Comments,  &nbsp&nbsp  R: Replies, &nbsp&nbsp   UP: Unique People engaged,  " +
                "&nbsp&nbsp  LU: Last Update, &nbsp&nbsp TSU: Time Since Update<br><br>" +
                "Simulated Time: " + sdf.format(new java.util.Date(simulatedTime)) + "<br><br>";

        long nActive = 0;
        long nInactive = 0;

        int nPosts = postIds.size();
        for (int i=0; i<nPosts; ++i){

            Integer post_id = postIds.get(i);
            PostStatistic ps = postStats.get(i);

            long timeSinceUpdate = simulatedTime - ps.getLastUpdate().getTimeInMillis();
            String stat = "Post " + post_id + ": &nbsp&nbsp" + ps.getnComments() + " C, &nbsp&nbsp"
                    + ps.getnReplies() + " R, &nbsp&nbsp" + ps.getnUniquePeopleEngaged() + " UP, &nbsp&nbsp " +
                    sdf.format(new java.util.Date(ps.getLastUpdate().getTimeInMillis())) +  " LU, &nbsp&nbsp "
                    + convert_ms(timeSinceUpdate)+ " TSU <br> ";

            if ( timeSinceUpdate < 12 * 3600 * 1000){
                activeBuilder.append(stat);
                nActive ++;
            } else {
                inactiveBuilder.append("(removed) " + stat);
                nInactive ++;
            }
        }

        return Title +
                "<h2> Active Posts (" + nActive + ") </h2>" +
                activeBuilder.toString();
                //"<h2> Inactive Posts in Memory (" + nInactive + ") </h2>" +
                //inactiveBuilder.toString();
    }

    private static String generate_acitivity_content(){

        List<Integer> postIds = new ArrayList<Integer>();
        List<PostStatistic> postStats = new ArrayList<PostStatistic>();

        get_aps_state_copy(postIds, postStats);

        StringBuilder csvBuilder = new StringBuilder();

        csvBuilder.append("post_id,last_update_in_millis,n_comments,n_replies,n_UniquePeopleEngaged,engaged_people_list(person,last_interaction)\n");


        int nPosts = postIds.size();
        for (int i=0; i<nPosts; ++i){

            int post_id = postIds.get(i);
            PostStatistic ps = postStats.get(i);

            csvBuilder.append(post_id + "," + ps.getLastUpdate().getTimeInMillis() + "," + ps.getnComments() + ","
                    + ps.getnReplies() + "," + ps.getnUniquePeopleEngaged());

            for (Integer p : ps.getAllPeopleEngaged()){
                csvBuilder.append("," + p);
                csvBuilder.append("," + ps.getLastInteraction(p).getTimeInMillis());
            }

            csvBuilder.append("\n");

        }



        return csvBuilder.toString();
    }

    private static void generate_post_statistic() throws IOException {

        // html file
        File htmlTemplateFile = new File("src/aps_template.html");
        String htmlString = FileUtils.readFileToString(htmlTemplateFile);

        String content = generate_aps_content();
        htmlString = htmlString.replace("$content", content);

        File newHtmlFile = new File("output/post_statistics/www.aps.dev.cc/index.html");
        FileUtils.writeStringToFile(newHtmlFile, htmlString);

        // csv file
        File csvFile = new File("output/post_stats.csv");
        String csv_content = generate_acitivity_content();

        FileUtils.writeStringToFile(csvFile, csv_content);

    }



    public void run() {
        System.out.println("ActivePostStats Started");

        do_setup();

        start_event_listener();
        System.out.println("Started event listeners");

        // update post statistic every 30 minutes
        int sleep_time = 30 * 60 * 1000 / SPEED_UP;
        while (true){
            try {
                currentThread().sleep(sleep_time);
                swapper.swap_out_inactive_posts();
                generate_post_statistic();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }
}
