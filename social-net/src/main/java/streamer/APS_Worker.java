package streamer;

import java.util.Calendar;
import java.util.HashMap;
import java.util.SortedMap;
import java.util.concurrent.locks.ReentrantLock;

class APS_Worker extends Thread {

    static SortedMap<Integer, PostStatistic> postIdToStat;
    static HashMap<Integer, Integer> postIdToOriginPostId;
    static ReentrantLock apsLock;
    static Calendar simulatedStart;
    static Calendar actualStart;
    static int SPEED_UP;

}
