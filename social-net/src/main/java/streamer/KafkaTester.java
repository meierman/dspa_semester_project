package streamer;

import socialnet.*;

public class KafkaTester extends Thread{

    public KafkaTester(){

    }

    private void runTestConsumer(){

        CommentKafkaConsumer commentConsumer
                = new CommentKafkaConsumer("ucTestGroup", IKafkaConstants.TOPIC_UNORDERED_COMMENTS);

        PostKafkaConsumer postConsumer
                = new PostKafkaConsumer("upTestGroup", IKafkaConstants.TOPIC_UNORDERED_POSTS);

        LikeKafkaConsumer likeConsumer
                = new LikeKafkaConsumer("ulTestGroup", IKafkaConstants.TOPIC_UNORDERED_LIKES);

        while (true){
            commentConsumer.readComments();
            postConsumer.readPosts();
            likeConsumer.readLikes();
        }
    }

    public void run() {
        System.out.println("Tester Started");
        runTestConsumer();
    }
}
