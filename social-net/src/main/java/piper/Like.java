package piper;

public class Like extends StreamObject {

	public int getPersonId() {
		return personId;
	}

	public int getPostId() {
		return postId;
	}

	public int getKey(){return 3;}

	private int personId;
	private int postId;
	
	public Like(String[] log){
		super(log[2]);
		personId = Integer.parseInt(log[0]);
		postId = Integer.parseInt(log[1]);
	}
}
