package piper;


import java.util.concurrent.PriorityBlockingQueue;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import socialnet.*;


public class StreamTicker extends Thread {
	PriorityBlockingQueue<StreamObject> likeQueue;
	PriorityBlockingQueue<StreamObject> commentQueue;
	PriorityBlockingQueue<StreamObject> postQueue;
	final int SPEED_UP;

	public StreamTicker(int speedup, PriorityBlockingQueue<StreamObject> likeq,
			PriorityBlockingQueue<StreamObject> commentq,
			PriorityBlockingQueue<StreamObject> postq) {
		SPEED_UP = speedup;
		likeQueue = likeq;
		commentQueue = commentq;
		postQueue = postq;
	}

	/**
	 * Reads Events from all three Queues and pushes them according to a simulated clock.
	 */
	public void run() {
		System.out.println("Ticker Started");

		//define producers
		CommentKafkaProducer commentProducer = new CommentKafkaProducer();
		PostKafkaProducer postProducer = new PostKafkaProducer();
		LikeKafkaProducer likeProducer = new LikeKafkaProducer();

		//Read first element for all three queues
		Like nextLike = null;
		Comment nextComment = null;
		Post nextPost = null;
		while ((nextLike = (Like) likeQueue.poll()) == null);
		while ((nextComment = (Comment) commentQueue.poll()) == null);
		while ((nextPost = (Post) postQueue.poll()) == null);

		// Simulated Start Time: Earliest arrival time of all streams
		StreamObject next = getNext(nextLike, nextComment, nextPost);
		long simulatedStart = next.getIngestionDate().getTimeInMillis();
		long startTime = System.currentTimeMillis(); // Used to simulate the time with speed up
        
        // Launch Friends Recommendation
		Recommender recm = new Recommender (SPEED_UP, simulatedStart, startTime);
		recm.start();
        
		SimpleDateFormat printFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		while (true) {
			long simulatedTime = simulatedStart + (System.currentTimeMillis() - startTime)*SPEED_UP;
			System.out.print("Time: "+printFormat.format(new Timestamp(simulatedTime)));
			
			if(next instanceof Like){
				//likeProducer.sendLike((Like) next, IKafkaConstants.TOPIC_UNORDERED_LIKES);
				likeProducer.sendLike((Like) next, IKafkaConstants.TOPIC_UNORDERED_LIKES);
				System.out.println(", Pushed Like    with Timestamp "+ printFormat.format(next.getIngestionDate().getTime()));
				while ((nextLike = (Like) likeQueue.poll()) == null);
			}else if (next instanceof Comment){
				//commentProducer.sendComment((Comment) next, IKafkaConstants.TOPIC_UNORDERED_COMMENTS);
				commentProducer.sendComment((Comment) next, IKafkaConstants.TOPIC_UNORDERED_COMMENTS);
				System.out.println(", Pushed Comment with Timestamp "+ printFormat.format(next.getIngestionDate().getTime()));
				while ((nextComment = (Comment) commentQueue.poll()) == null);
			}else {
				//postProducer.sendPost((Post) next, IKafkaConstants.TOPIC_UNORDERED_POSTS);
				postProducer.sendPost((Post) next, IKafkaConstants.TOPIC_UNORDERED_POSTS);
				System.out.println(", Pushed Post    with Timestamp "+ printFormat.format(next.getIngestionDate().getTime()));
				while ((nextPost = (Post) postQueue.poll()) == null);
			}
			
			//Find next element that is up amongst the three heads of the queues
			next = getNext(nextLike, nextComment, nextPost);
			double wait_ms = (double)(next.getIngestionDate().getTimeInMillis() - simulatedTime)/SPEED_UP;
			
			//Wait until next element is up
			if (wait_ms > 0) {
				try {
					Thread.sleep((int)wait_ms, (int) ((wait_ms%1) * 1000000));
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * 
	 * @param lk
	 * @param cm
	 * @param ps
	 * @return The next event in a set of a Like, Comment and Post
	 */
	StreamObject getNext(Like lk, Comment cm, Post ps) {
		if (lk.getIngestionDate().compareTo(cm.getIngestionDate()) <= 0) {
			if (lk.getIngestionDate().compareTo(ps.getIngestionDate()) <= 0) {
				return lk;
			}
			return ps;
		}
		if (cm.getIngestionDate().compareTo(ps.getIngestionDate()) <= 0) {
			return cm;
		}
		return ps;
	}
}
