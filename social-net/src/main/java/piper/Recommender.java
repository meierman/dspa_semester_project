package piper;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.PrintWriter;

import java.sql.Timestamp;

public class Recommender extends Thread {
	static HashMap<Integer, PotentialFriend> users = new HashMap<Integer, PotentialFriend>();
	static final String csvTablePerson = "../1k-users-sorted/tables/person.csv";
	static final String csvTableForumMember = "../1k-users-sorted/tables/forum_hasMember_person.csv";
	static final String csvTableForumModerator = "../1k-users-sorted/tables/forum_hasModerator_person.csv";
	static final String csvTableStudyAt = "../1k-users-sorted/tables/person_studyAt_organisation.csv";
	static final String csvTableFriendslist = "../1k-users-sorted/tables/person_knows_person.csv";
	static final String csvTablePostStats = "./output/post_stats.csv";
	static final int[] focusgroup = { 833, 7, 919, 100, 545, 915, 534, 941, 3,
			78 }; // IDs of people to generate recommendations for
	int Speed_up;
	long Simulation_start_realtime;
	long Simulation_startpoint;

	public Recommender(int speedup, long sim_startpoint, long sim_start_realtime) {
		Speed_up = speedup;
		Simulation_start_realtime = sim_start_realtime;
		Simulation_startpoint = sim_startpoint;

		// Analyze Data that is assumed to be static: Userlist, Friendships,
		// University-association, Forum memberships
		BufferedReader br = null;
		String line = "";

		try {
			System.out.println("Creating User-Hashmap");
			br = new BufferedReader(new FileReader(csvTablePerson));
			line = br.readLine(); // Ignore Headers
			while ((line = br.readLine()) != null) {
				String[] elements = line.split("\\|");
				users.put(Integer.parseInt(elements[0]), new PotentialFriend(
						elements));
			}
			br.close();

			System.out.println("Adding University Info");
			br = new BufferedReader(new FileReader(csvTableStudyAt));
			line = br.readLine(); // Ignore Headers
			while ((line = br.readLine()) != null) {
				String[] elements = line.split("\\|");
				users.get(Integer.parseInt(elements[0])).setUniversity(
						Integer.parseInt(elements[1]),
						Integer.parseInt(elements[2]));
			}
			br.close();

			System.out.println("Adding Friendlist Info");
			br = new BufferedReader(new FileReader(csvTableFriendslist));
			line = br.readLine(); // Ignore Headers
			while ((line = br.readLine()) != null) {
				String[] elements = line.split("\\|");
				users.get(Integer.parseInt(elements[0])).addFriend(
						Integer.parseInt(elements[1]));
			}
			br.close();

			System.out.println("Adding Forum Info");
			br = new BufferedReader(new FileReader(csvTableForumMember));
			line = br.readLine(); // Ignore Headers
			while ((line = br.readLine()) != null) {
				String[] elements = line.split("\\|");
				if (elements.length >= 2) {
					PotentialFriend pf = users.get(Integer
							.parseInt(elements[1]));
					if (pf == null) {
						System.out.println("Error: Person not found");
						continue;
					}
					pf.addForum(Integer.parseInt(elements[0]));
				} else
					System.out.println("Error: " + line);
			}
			br.close();
			br = new BufferedReader(new FileReader(csvTableForumModerator));
			line = br.readLine(); // Ignore Headers
			while ((line = br.readLine()) != null) {
				String[] elements = line.split("\\|");
				users.get(Integer.parseInt(elements[1])).addForum(
						Integer.parseInt(elements[0]));
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void run() {
		BufferedReader br = null;
		String line = "";
		SimpleDateFormat printFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");

		while (true) {
			long currSimTime = Simulation_startpoint
					+ (System.currentTimeMillis() - Simulation_start_realtime)
					* Speed_up;
			String currTime = "Simulated Time: " + printFormat.format(new Timestamp(currSimTime));
//			System.out.println(currTime);
//			System.out.println("Computing Recommendations");

			try {

				for (PotentialFriend currentPerson : users.values()) {
					currentPerson.active = false;
					currentPerson.clearPosts();
				}

				br = new BufferedReader(new FileReader(csvTablePostStats));
				line = br.readLine(); // Ignore Headers
				while ((line = br.readLine()) != null) {
					String[] elements = line.split(",");
					int post_id = Integer.parseInt(elements[0]);
					for (int i = 5; i < elements.length; i += 2) {
						PotentialFriend pf = users.get(Integer
								.parseInt(elements[i]));

						if (Long.parseLong(elements[i + 1]) > currSimTime - 12 * 3600 * 1000) {
							pf.active = true;
						}
						if (Long.parseLong(elements[i + 1]) > currSimTime - 4 * 3600 * 1000) {
							pf.addPost(post_id);
						}
					}
				}
				br.close();

				String user_recos = new String("<br>"+currTime);
				for (int i : focusgroup) {
					user_recos += getRecommendations(i);
				}

				// File htmlTemplateFile = new File("src/aps_template.html");
				byte[] encoded = Files.readAllBytes(Paths
						.get("src/aps_template_friends.html"));
				String htmlString = new String(encoded,
						Charset.defaultCharset());

				String content = user_recos;
				htmlString = htmlString.replace("$content", content);

				PrintWriter out = new PrintWriter(
						"output/post_statistics/www.friends.dev.cc/index.html");
				out.println(htmlString);
				out.close();

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			currSimTime = Simulation_startpoint
					+ (System.currentTimeMillis() - Simulation_start_realtime)
					* Speed_up;
			double wait_ms = (double) (3600000 - currSimTime % 3600000)
					/ Speed_up;

			// Wait until next element is up
			if (wait_ms > 0) {
				try {
					Thread.sleep((int) wait_ms, (int) ((wait_ms % 1) * 1000000));
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

		}

	}

	static private String getRecommendations(int user_id) {
		String returnstr = new String();
		PotentialFriend thisPerson = users.get(user_id);
		ArrayList<ScoredPotentialFriend> recos = new ArrayList<ScoredPotentialFriend>();
		ArrayList<Integer> max_scores = new ArrayList<Integer>();
		max_scores.add(0);
		max_scores.add(0);
		max_scores.add(0);
		max_scores.add(0);
		max_scores.add(0);
		for (PotentialFriend currentPerson : users.values()) {
			// Candidate if not himself or already friend TODO: Active
			if (currentPerson.active
					&& !thisPerson.Friends.contains(currentPerson.userId)
					&& thisPerson.userId != currentPerson.userId) {
				ScoredPotentialFriend spf = new ScoredPotentialFriend(0,
						currentPerson);
				spf.getScore(thisPerson);
				getMaxList(max_scores, spf.score_parts);
				recos.add(spf);
			}
		}
		for(ScoredPotentialFriend spf : recos){
			spf.normalize_score(max_scores);
		}
		recos.sort(Comparator.comparing(ScoredPotentialFriend::getScore));
		returnstr += "<h3>Recommendations for User "
				+ Integer.toString(user_id)
				+ "</h3><br/><table style='border-spacing:50px 0; border-collapse: separate;'> "
				+ "<tr><th>User</th><th>Score</th><th>Same University</th><th>Same Year</th><th>Mutual Forums</th><th>Mutual Friends</th><th>Mutual Post Activity</th></tr>";
		for (int i = recos.size() - 1; i >= 0 && i >= recos.size() - 5; i--) {
			returnstr += "<tr><td>User "
					+ Integer.toString(recos.get(i).potFriend.userId)
					+ "</td><td>" + String.format("%.2f", recos.get(i).score)
					+ "</td><td>" + recos.get(i).score_parts.get(0)
					+ "</td><td>" + recos.get(i).score_parts.get(1)
					+ "</td><td>" + recos.get(i).score_parts.get(2)
					+ "</td><td>" + recos.get(i).score_parts.get(3)
					+ "</td><td>" + recos.get(i).score_parts.get(4)
					+ "</td><td></tr>";
		}
		returnstr += "</table><br><br>";
		return returnstr;
	}
	
	private static void getMaxList (ArrayList<Integer> maxlist, ArrayList<Integer> newlist){
		for(int i = 0; i<maxlist.size();i++){
			maxlist.set(i, Math.max(maxlist.get(i), newlist.get(i)));
		}
	}
	

	private static Calendar CreateDateFromStr(String ts) {
		Calendar timeStamp = Calendar.getInstance();
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat(
					"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			timeStamp.setTime(dateFormat.parse(ts));
		} catch (Exception e) { // this generic but you can control another
								// types of exception

			try {
				SimpleDateFormat dateFormat = new SimpleDateFormat(
						"yyyy-MM-dd'T'HH:mm:ss'Z'");
				timeStamp.setTime(dateFormat.parse(ts));
			} catch (Exception e2) { // this generic but you can control another
										// types of exception
				System.out.println(e.getMessage());
				System.out.println(e2.getMessage());
			}
		}
		return timeStamp;

	}

}
