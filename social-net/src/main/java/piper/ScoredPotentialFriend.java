package piper;

import java.util.ArrayList;

public class ScoredPotentialFriend {
	/*public static final double SAME_UNI = 0.5; 
	public static final double SAME_YEAR = 0.5; // Additional Bonus for Same Year same uni
	public static final double SAME_FORUM = 0.005; //Score for each shared forum membership
	public static final double MUTUAL_FRIEND = 0.02; //Score for each shared forum membership
	public static final double SAME_POST_ACTIVITY = 0.5; //Score for each shared post activity in the past 4 hours
	public static double[] scores = {SAME_UNI, SAME_YEAR, SAME_FORUM, MUTUAL_FRIEND, SAME_POST_ACTIVITY};*/
	public static double[] max_scores_const = {0.5, 0.5, 1, 1, 1.5};
	
	
	double score;
	public ArrayList<Integer> score_parts;
	
	PotentialFriend potFriend;
	public ScoredPotentialFriend(double s, PotentialFriend p){
		score = s;
		potFriend = p;		
	}
	public double getScore(){
		return score;
	}

	public double getScore(PotentialFriend referencePerson){
		score_parts = potFriend.getScore(referencePerson);
		double s = 0;
		for(int i=0 ; i<score_parts.size() ; i++) {
			s += score_parts.get(i);//*scores[i];
		}
		score = s;
		return s;
	}
	public double normalize_score(ArrayList<Integer> max_scores){
		double s = 0;
		for(int i=0 ; i<score_parts.size() ; i++) {
			if(max_scores.get(i)>0){
				s += (double)score_parts.get(i)*max_scores_const[i]/max_scores.get(i);
			}
		}
		score = s;
		return s;
	}
}
