package piper;

import streamer.*;


import java.util.Comparator;
import java.util.concurrent.PriorityBlockingQueue;



public class Main {
	static final boolean runTest = false;

	static final int QUEUE_SIZE = 10;

	static final String csvFileLike = "../1k-users-sorted/streams/likes_event_stream_filtered.csv";
	static final String csvFilePost = "../1k-users-sorted/streams/post_event_stream_filtered.csv";
	static final String csvFileComment = "../1k-users-sorted/streams/comment_event_stream_filtered.csv";


    static Comparator<StreamObject> ingestionComp = new Comparator<StreamObject>() {
        @Override
        public int compare(StreamObject p1, StreamObject p2) {
            if (p1.getIngestionDate().compareTo(p2.getIngestionDate()) > 0) {
                return 1;
            } else if (p1.getIngestionDate().compareTo(p2.getIngestionDate()) < 0) {
                return -1;
            }
            return 0;
        }
    };

	private static void feedDataToKafka(){

		PriorityBlockingQueue<StreamObject> likeQueue = new PriorityBlockingQueue<StreamObject> (QUEUE_SIZE, ingestionComp);
		StreamReader LikeReader = new StreamReader (csvFileLike, likeQueue, QUEUE_SIZE);
		LikeReader.start();
		PriorityBlockingQueue<StreamObject> commentQueue = new PriorityBlockingQueue<StreamObject> (QUEUE_SIZE, ingestionComp);
		StreamReader CommentReader = new StreamReader (csvFileComment, commentQueue, QUEUE_SIZE);
		CommentReader.start();
		PriorityBlockingQueue<StreamObject> postQueue = new PriorityBlockingQueue<StreamObject> (QUEUE_SIZE, ingestionComp);
		StreamReader PostReader = new StreamReader (csvFilePost, postQueue, QUEUE_SIZE);
		PostReader.start();

		StreamTicker streamTick = new StreamTicker (IPiperConstants.SPEED_UP, likeQueue, commentQueue, postQueue);
		streamTick.start();

	}

	private static void runKafkaTests(){
		KafkaTester kt = new KafkaTester();
		kt.start();
	}

	private static void runPostStatistic(){
		ActivePostStats aps = new ActivePostStats(IPiperConstants.SPEED_UP);
		aps.start();
	}

	private static void reorderData(){
		Orderer orderer = new Orderer(IPiperConstants.MAX_DELAY_MS);
		orderer.start();
	}


	
	public static void main(String[] args) {
		feedDataToKafka();

		reorderData();

		if (runTest){
			runKafkaTests();

			return;
		}

		runPostStatistic();
	}

}
