package piper;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ArrayList;


public class PotentialFriend {

	
	
	int userId;
	Calendar created;
	private int _OrganisationId;
	private int _ClassYear;
	public ArrayList<Integer> Friends;
	public ArrayList<Integer> Forums;
	public ArrayList<Integer> Posts;
	public boolean active;
	
	public PotentialFriend(String[] line_in_file){
		userId = Integer.parseInt(line_in_file[0]);
		created = CreateDateFromStr(line_in_file[5]);
		Friends = new ArrayList<Integer>();
		Forums = new ArrayList<Integer>();
		Posts = new ArrayList<Integer>();
		active = false;
	}

	public void setUniversity(int uni, int year){
		_OrganisationId = uni;
		_ClassYear = year;
	}
	public void addFriend(int id){
		Friends.add(id);
	}
	public void addForum(int id){
		Forums.add(id);
	}
	public void addPost(int id){
		Posts.add(id);
	}
	public void clearPosts(){
		Posts.clear();
	}
	
	/**
	 * 
	 * @param referencePerson
	 * @return List: [bool SameUni, bool SameYear, int forums, int friends]
	 */
	public ArrayList<Integer> getScore(PotentialFriend referencePerson){
		ArrayList<Integer> score = new ArrayList<Integer>();
		if(referencePerson._OrganisationId == this._OrganisationId){
			score.add(1);
			if(referencePerson._ClassYear == this._ClassYear)
				score.add(1);
			else
				score.add(0);
		}
		else{
			score.add(0);
			score.add(0);
		}
		
		int forums = 0;
		for(int forum : referencePerson.Forums){
			if(this.Forums.contains(forum)){
				forums++;
			}
		}
		score.add(forums);

		int friends=0;
		for(int friend : referencePerson.Friends){
			if(this.Friends.contains(friend)){
				friends++;
			}
		}
		score.add(friends);
		
		int post_activity=0;
		for(int friend : referencePerson.Posts){
			if(this.Posts.contains(friend)){
				post_activity++;
			}
		}
		score.add(post_activity);
		
		return score;
	}
	private static Calendar CreateDateFromStr(String ts) {
		Calendar timeStamp = Calendar.getInstance();
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat(
					"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			timeStamp.setTime(dateFormat.parse(ts));
		} catch (Exception e) { // this generic but you can control another
								// types of exception

			try {
				SimpleDateFormat dateFormat = new SimpleDateFormat(
						"yyyy-MM-dd'T'HH:mm:ss'Z'");
				timeStamp.setTime(dateFormat.parse(ts));
			} catch (Exception e2) { // this generic but you can control another
									// types of exception
				System.out.println(e.getMessage());
				System.out.println(e2.getMessage());
			}
		}
		return timeStamp;
		
	}
	
}
