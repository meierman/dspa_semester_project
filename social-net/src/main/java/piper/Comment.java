package piper;

public class Comment extends StreamObject {
	private int id, personId, reply_to_postId, reply_to_commentId, placeId;
	String locationIP, browserUsed, content;

	public int getId() {
		return id;
	}

	public int getPersonId() {
		return personId;
	}

	public int getReply_to_postId() {
		return reply_to_postId;
	}

	public int getReply_to_commentId() {
		return reply_to_commentId;
	}

	public int getPlaceId() {
		return placeId;
	}

	public String getLocationIP() {
		return locationIP;
	}

	public String getBrowserUsed() {
		return browserUsed;
	}

	public String getContent() {
		return content;
	}

	public int getKey(){return 2;}

	public Comment(String[] log){
		super(log[2]);
		id=Integer.parseInt(log[0]);
		personId=Integer.parseInt(log[1]);
		locationIP=log[3];
		browserUsed=log[4];
		content=log[5];
		if(log[6].length()>0)
			reply_to_postId=Integer.parseInt(log[6]);
		else
			reply_to_postId=-1;
		if(log[7].length()>0)
			reply_to_commentId=Integer.parseInt(log[7]);
		else
			reply_to_commentId=-1;
		
		placeId=Integer.parseInt(log[8]);
	}

}
