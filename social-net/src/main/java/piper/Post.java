package piper;

public class Post extends StreamObject {
	public int getId() {
		return id;
	}

	public int getPersonId() {
		return personId;
	}

	public int getForumId() {
		return forumId;
	}

	public int getPlaceId() {
		return placeId;
	}

	public String getImageFile() {
		return imageFile;
	}

	public String getLocationIP() {
		return locationIP;
	}

	public String getBrowserUsed() {
		return browserUsed;
	}

	public String getLanguage() {
		return language;
	}

	public String getContent() {
		return content;
	}

	public String getTags() {
		return tags;
	}

	public int getKey(){return 1;}

	private int id, personId, forumId, placeId;
	String imageFile, locationIP, browserUsed, language, content, tags;
	
	public Post(String[] log){
		super(log[2]);
		id=Integer.parseInt(log[0]);
		personId=Integer.parseInt(log[1]);
		imageFile=log[3];
		locationIP=log[4];
		browserUsed=log[5];
		language=log[6];
		content=log[7];
		tags=log[8];
		forumId=Integer.parseInt(log[9]);
		placeId=Integer.parseInt(log[10]);
	}
}