package piper;

public interface IPiperConstants {

    static final int MAX_DELAY_MS = 5000;
    static final int SPEED_UP = 3600;
}
