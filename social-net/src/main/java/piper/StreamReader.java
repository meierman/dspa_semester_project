package piper;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.PriorityBlockingQueue;

public class StreamReader extends Thread {
	String csvPath;
	PriorityBlockingQueue<StreamObject> streamQueue;
	final int MAX_QUEUE_LENGTH;
	
	
	public StreamReader(String path, PriorityBlockingQueue<StreamObject> q, int max_q){
		csvPath = path;
		streamQueue = q;
		MAX_QUEUE_LENGTH = max_q;
	}
	
	/**
	 * Reads a stream-csv file, creates an object for each row and inserts it into the respective queue
	 */
	public void run(){
		BufferedReader br = null;
		String line = "";
		try {
			br = new BufferedReader(new FileReader(csvPath));
			line = br.readLine(); // Ignore Headers
			
			while ((line = br.readLine()) != null) {
				String[] elements = line.split("\\|");
				if(elements.length == 3){
					Like lk = new Like(elements);
					streamQueue.add(lk);
				} else if(elements.length == 11){
					Post lk = new Post(elements);
					streamQueue.add(lk);
				} else if(elements.length == 9){
					Comment lk = new Comment(elements);
					streamQueue.add(lk);					
				}
				//System.out.println("Read:" + line);
				while(streamQueue.size()>=MAX_QUEUE_LENGTH){
					try{
						Thread.sleep(1);
					}catch(InterruptedException e){
						e.printStackTrace();
					}
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
