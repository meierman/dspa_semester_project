package piper;

import java.util.Calendar;
import java.util.Random;
import java.text.SimpleDateFormat;

public class StreamObject implements Comparable<StreamObject> {


	private Calendar creationDate;
	private Calendar ingestionDate;
	static Random rand = new Random();


	public Calendar getCreationDate() {
		return creationDate;
	}
	public Calendar getIngestionDate() {
		return ingestionDate;
	}

	public StreamObject(String cDate) {
		creationDate = Calendar.getInstance();
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat(
					"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			creationDate.setTime(dateFormat.parse(cDate));
		} catch (Exception e) { // this generic but you can control another
								// types of exception

			try {
				SimpleDateFormat dateFormat = new SimpleDateFormat(
						"yyyy-MM-dd'T'HH:mm:ss'Z'");
				creationDate.setTime(dateFormat.parse(cDate));
			} catch (Exception e2) { // this generic but you can control another
									// types of exception
				System.out.println(e.getMessage());
				System.out.println(e2.getMessage());
			}
		}
		ingestionDate = (Calendar) creationDate.clone();
		ingestionDate.add(Calendar.MILLISECOND, rand.nextInt(IPiperConstants.MAX_DELAY_MS));

	}

	public int compareTo(StreamObject other) {
		if (creationDate.compareTo(other.creationDate) > 0) {
			return 1;
		} else if (creationDate.compareTo(other.creationDate) < 0) {
			return -1;
		}
		return 0;
	}

}