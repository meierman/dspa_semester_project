# Social Network Simulation


## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

Before you can run the application, ensure to have running the following setup:

#### 1. Kafka
Run Zookeeper and Kafka from your Kafka root folder.
```
bin/zookeeper-server-start.sh config/zookeeper.properties
bin/kafka-server-start.sh config/server.properties
```

#### 2. Kafka Topics
Add the needed topics in Kafka. Required Topics are:

* unordered_posts
* unordered_comments
* unordered_likes
* posts
* comments
* likes

```
bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic unordered_posts
bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic unordered_comments
bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic unordered_likes

bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic posts
bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic comments
bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic likes
```

#### 3. MySQL Database
You need to have running a MySQL database on localhost with name "aps" and user "root" to which the JDBC can connect.
We used the application [DesktopServer](https://serverpress.com/downloads/), as we were already familiar with it and
it makes the simulation of a website for the post statistics and friends recommendations easy to use and databases
can be configured and inspected quickly.

In DesktopServer, you have to "Create a new development website" (we chose www.aps.dev.cc"), go to "Sites" and hit
"Database". This opens phpMyAdmin in the browser, where you can create the new database as described before.
It should however also be possible to run any different application in which you can setup a database as described
before.
The database is used to archive inactive posts. Whenever a post becomes inactive, it is swapped out to the database, as
soon as it gets updated, it is swapped into memory again.

Note: Our application connects to the database in the following way
```
[...]
DriverManager.getConnection("jdbc:mysql://localhost/aps?user=root");
[...]
```

#### 4. Data
Add the folder with 1k-user-data to the root directory.
```
dspa_semester_project/1k-users-sorted
```

As the provided datasets have errors in the timestamps, we implemented a data normalizer which filters out all invalid events.

Run the data normalizer (dspa_semester_project/data_normalizer) if you haven't done so. More details
on how to run the data normalizer can be found [here](./data_normalizer). There is also a download link provided for the big data set.

The data folder structure should now contain the following files:
```
1k-users-sorted/streams/comment_event_stream_filtered.csv
1k-users-sorted/streams/likes_event_stream_filtered.csv
1k-users-sorted/streams/post_event_stream_filtered.csv
10k-users-sorted/streams/comment_event_stream_filtered.csv
10k-users-sorted/streams/likes_event_stream_filtered.csv
10k-users-sorted/streams/post_event_stream_filtered.csv

```

### 5. Configurations
Before compiling and running the code, configurations may be changed in the source code:

* Location of the data stream files: In [Main.java](./src/main/java/piper/Main.java)
* Speed-up factor for the simulated time: `SPEED_UP` constant in [Main.java](./src/main/java/piper/Main.java)
* Queue sizes for the Kafka feeders: `QUEUE_SIZE` constant in [Main.java](./src/main/java/piper/Main.java). Note: The bigger the speed-up factor and the denser the datastreams, the bigger the ordered queues should be. In the final source-commit, it was set to 10. For the big data set, we recommend setting it to 1000.
* Maximum delay between event time and ingestion time in Milliseconds: `MAX_DELAY_MS` constant in  [StreamObject.java](./src/main/java/piper/StreamObject.java). Note: Setting it to 1 means, that no Delay is added and events arrive in-order.
* Location of the various .csv tables: In [Recommender.java](./src/main/java/piper/Recommender.java)
* Configuration of the users for whom recommendations should be computed: In the `focusGroup` Array in [Recommender.java](./src/main/java/piper/Recommender.java) (Note: An arbitrary number of user id's may be added to this array)
* Configuration of the weights of the different sub-scores for the user recommendations: In the `max_score_const` Array in [ScoredPotentialFriend.java](./src/main/java/piper/ScoredPotentialFriend.java) (Array indices: 0 - Score for being at the same uni, 1 - Score for graduating the same year, 2 - Score for being member of identical forums, 3 - Score for having mutual friends, 4 - Score for activities on the same posts)

#### 6. Local Website (optional)
To display the Active Post Statistics and the Friends Recommendations the application periodically updates two index.html files
which can be run in a browser. The two files can be found here:

```
output/post_statistics/www.aps.dev.cc/index.html
output/post_statistics/www.friends.dev.cc/index.html
```

Using DesktopServer, one can easily setup two local websites "www.aps.dev.cc" and "www.friends.dev.cc" to simulate the access
of the Active Post Statistics and Friends Recommendations by URL. However, the two index.html files can also be opened and inspected directly.


### 7. Running the app

Open your IDE and import the Maven project social-net. Select the following file on import:

```
social-net/pom.xml
```

The main class to run is the following:
```
social-net/src/main/java/piper/Main.java
```

Note: If you rerun the application, ensure that the kafka topics are empty again, as there might be old unconsumed events
stored in the kafka partitions, which would lead to an inconsistent ordering of the events.