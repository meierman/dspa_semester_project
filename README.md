# DSPA Semester Project

#### Contributors

* Manuel Meier (14-914-766)
* Fabian Müller (12-927-448)

## Setup and Run

Before you can run the application, you need to setup your local environment and preprocess the input data.
The preprocessing (data normalizer) and the main application (social network simulation) are two independent applications.


1. Follow the instructions to preprocess the data:
* [Data Preprocessing](./data_normalizer)

2. Follow the instructions to setup your local environment and run the application:
* [Social Network Simulation](./social-net)