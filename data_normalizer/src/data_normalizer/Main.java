package data_normalizer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.Calendar;
import java.util.HashMap;
import java.util.ArrayList;
import java.text.SimpleDateFormat;



public class Main {

	public static void main(String[] args){
		String csvFileLike = "../1k-users-sorted/streams/likes_event_stream.csv";
		String csvFileLike_new = "../1k-users-sorted/streams/likes_event_stream_filtered.csv";
		String csvFilePost = "../1k-users-sorted/streams/post_event_stream.csv";
		String csvFilePost_new = "../1k-users-sorted/streams/post_event_stream_filtered.csv";
		String csvFileComment = "../1k-users-sorted/streams/comment_event_stream.csv";
		String csvFileComment_new = "../1k-users-sorted/streams/comment_event_stream_filtered.csv";
		
		BufferedReader br = null;
		String line = "";

		HashMap<Integer, Calendar> posts = new HashMap<Integer, Calendar>();
		HashMap<Integer, Calendar> comments = new HashMap<Integer, Calendar>();
		ArrayList<Integer> postsInvalid = new ArrayList<Integer>();
		ArrayList<Integer> commentsInvalid = new ArrayList<Integer>();
		
		try {
			
			System.out.println("Creating HashMap with all Posts");
			// Create HashMap from Posts
			br = new BufferedReader(new FileReader(csvFilePost));
			line = br.readLine(); // Ignore Headers
			while ((line = br.readLine()) != null) {
				String[] elements = line.split("\\|");
				posts.put(Integer.parseInt(elements[0]), CreateDateFromStr(elements[2]));			
			}	
			br.close();

			System.out.println("Checking for Likes that arrive before Posts");
			// Load likes and check if respective Posts already exist
			br = new BufferedReader(new FileReader(csvFileLike));
			line = br.readLine(); // Ignore Headers
			while ((line = br.readLine()) != null) {
				String[] elements = line.split("\\|");
				int postId = Integer.parseInt(elements[1]);
				Calendar post_time = posts.get(postId);
				if(CreateDateFromStr(elements[2]).before(post_time)){
					//System.out.println("Invalid Post " + elements[1] + " created " + post_time.getTimeInMillis() + " with Like at " + CreateDateFromStr(elements[2]).getTimeInMillis());
					if(!postsInvalid.contains(postId)){
						postsInvalid.add(postId);
					}
				}		
			}	
			br.close();

			System.out.println("Checking for Comments that arrive before Posts or parent Comments");
			// Load Comments and check if respective Posts / or Comments already exist
			br = new BufferedReader(new FileReader(csvFileComment));
			line = br.readLine(); // Ignore Headers
			while ((line = br.readLine()) != null) {
				String[] elements = line.split("\\|");
				comments.put(Integer.parseInt(elements[0]), CreateDateFromStr(elements[2]));

				// If comment to post check post time
				if(elements[6].length()>0){
					int postId = Integer.parseInt(elements[6]);
					Calendar post_time = posts.get(postId);
					if(CreateDateFromStr(elements[2]).before(post_time)){
						//System.out.println("Invalid Post " + elements[6] + " created " + post_time.getTimeInMillis() + "with Comment at " + CreateDateFromStr(elements[2]).getTimeInMillis());
						if(!postsInvalid.contains(postId)){
							postsInvalid.add(postId);
						}
					}
				}
				
				// If reply to comment check comment time
				else if(elements[7].length()>0){
					int commentId = Integer.parseInt(elements[7]);
					Calendar comment_time = comments.get(commentId);
					if(CreateDateFromStr(elements[2]).before(comment_time) || comment_time == null){
						//System.out.println("Invalid Comment " + elements[6] + " created " + comment_time.getTimeInMillis() + "with Reply at " + CreateDateFromStr(elements[2]).getTimeInMillis());
						if(!commentsInvalid.contains(Integer.parseInt(elements[0]))){
							commentsInvalid.add(Integer.parseInt(elements[0]));
						}
					}
				}	
				else{
					System.out.println("ERROR, Comment " + elements[0] + "REFERENCES NOTHING!");
					break;
				}
			}
			
			System.out.println("Found " + postsInvalid.size() + " invalid Posts and "+ commentsInvalid.size() +" invalid Comments");
			
			// Empty RAM
			posts.clear();
			comments.clear();


			System.out.println("Delete Rows from Posts File");
			File inputFile = new File(csvFilePost);
			File tempFile = new File(csvFilePost_new);
			BufferedReader reader = new BufferedReader(new FileReader(inputFile));
			BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));
			String currentLine=reader.readLine();
			writer.write(currentLine + System.getProperty("line.separator"));
			
			while((currentLine = reader.readLine()) != null) {
			    // trim newline when comparing with lineToRemove
				String[] elements = currentLine.split("\\|");
				
			    if(postsInvalid.contains(Integer.parseInt(elements[0]))) continue;
			    writer.write(currentLine + System.getProperty("line.separator"));
			}
			writer.close(); 
			reader.close();
			
			
			System.out.println("Delete Rows from Likes File");
			inputFile = new File(csvFileLike);
			tempFile = new File(csvFileLike_new);
			reader = new BufferedReader(new FileReader(inputFile));
			writer = new BufferedWriter(new FileWriter(tempFile));
			int deleted_likes = 0;
			currentLine=reader.readLine();
			writer.write(currentLine + System.getProperty("line.separator"));
			
			while((currentLine = reader.readLine()) != null) {
			    // trim newline when comparing with lineToRemove
				String[] elements = currentLine.split("\\|");
				
			    if(postsInvalid.contains(Integer.parseInt(elements[1]))){
			    	deleted_likes++;
			    	continue;
			    }
			    writer.write(currentLine + System.getProperty("line.separator"));
			}
			writer.close(); 
			reader.close();	
			System.out.println(deleted_likes + " Likes Deleted");
			
			
			System.out.println("Delete Rows from Comments File");
			ArrayList<Integer> deletedComments = new ArrayList<Integer>();
			inputFile = new File(csvFileComment);
			tempFile = new File(csvFileComment_new);
			reader = new BufferedReader(new FileReader(inputFile));
			writer = new BufferedWriter(new FileWriter(tempFile));
			currentLine=reader.readLine();
			writer.write(currentLine + System.getProperty("line.separator"));
			int deleted_comments = 0;
			while((currentLine = reader.readLine()) != null) {
			    // trim newline when comparing with lineToRemove
				String[] elements = currentLine.split("\\|");
				int thiscomment = Integer.parseInt(elements[0]);
				//If comment delete if post is deleted or comment invalid
				
				if(elements[6].length()>0){
					int postId = Integer.parseInt(elements[6]);
					if(postsInvalid.contains(postId) ||
							commentsInvalid.contains(thiscomment)){ 
						deletedComments.add(thiscomment);
						deleted_comments++;
						continue;
					}
				}
				//If reply delete if comment is deleted or invalid
				else{
					int commId = Integer.parseInt(elements[7]);
					if(deletedComments.contains(commId) ||
							commentsInvalid.contains(commId) ||
							commentsInvalid.contains(thiscomment)){ 
						deletedComments.add(thiscomment);
						deleted_comments++;
						continue;
					}
				}
			    writer.write(currentLine + System.getProperty("line.separator"));
			}
			writer.close(); 
			reader.close();
			System.out.println(deleted_comments + " Comments Deleted");
			
			
			
			

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	
	private static Calendar CreateDateFromStr(String ts) {
		Calendar timeStamp = Calendar.getInstance();
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat(
					"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			timeStamp.setTime(dateFormat.parse(ts));
		} catch (Exception e) { // this generic but you can control another
								// types of exception

			try {
				SimpleDateFormat dateFormat = new SimpleDateFormat(
						"yyyy-MM-dd'T'HH:mm:ss'Z'");
				timeStamp.setTime(dateFormat.parse(ts));
			} catch (Exception e2) { // this generic but you can control another
									// types of exception
				System.out.println(e.getMessage());
				System.out.println(e2.getMessage());
			}
		}
		return timeStamp;
		
	}
	
	
}
