# Data Cleaning

As the provided datasets contain errors in the timestamps, we implemented a data normalizer which filters out all invalid events.

### Prerequisites

Ensure that you have downloaded the input data. Put the folder "1k-users-sorted" into the root directory.

```
dspa_semester_project/1k-users-sorted
```

### Run the data preprocessing

Open your IDE and import the project data_normalizer. It contains only one Main class which you should run. The preprocessing
can take a few minutes to run.

After the preprocessing has finished, the data folder structure should contain the following files:
```
1k-users-sorted/streams/comment_event_stream_filtered.csv
1k-users-sorted/streams/likes_event_stream_filtered.csv
1k-users-sorted/streams/post_event_stream_filtered.csv
```

Or alternatively the files of the big data-set. Configuring the file paths is described [here](./social-net)


### Download preprocessed data

The preprocessing was not optimized as it is not part of the task set of this project. Therefore, preprocessing the big dataset takes a lot of time and computing power. Alternatively you can download the preprocessed set here:
[Big Data Set Preprocessed](https://polybox.ethz.ch/index.php/s/in9rLEe5VTO8Glz)